﻿using Func_DLL;
using System;
using System.Collections;
using System.Linq;

namespace BPSS_DLL
{
    public class BPSS : BaseCom
    {
        #region Private
        public byte _fpsAddressReceiver = 0x02;
        private byte _addressSender;
        private byte _addressReceiver;
        private byte _counter;
        #endregion

        #region Event`s
        public event EventHandler<ConfirmSetEventArgs> OnConfirmSet;
        public event EventHandler<ConfirmStatusEventArgs> OnConfirmStatus;
        public event EventHandler<ConfirmAntennaStatusEventArgs> OnConfirmAntennaStatus;
        #endregion

        #region Constr, SetServicePart, EventOnReadByte 
        public BPSS(byte AddressAWP, byte AddressBPPS)
        {
            _addressSender = AddressAWP;
            _addressReceiver = AddressBPPS;
            OnReadByte += BPPS_OnReadByte;
        }

        private void BPPS_OnReadByte(object sender, byte[] e)
        {
            byte[] bBufSave = new byte[LEN_ARRAY];
            int iTempLength = 0;
            bool blExistCmd = true;

            try
            {
                Array.Resize(ref bBufSave, iTempLength + e.Length);
                Array.Copy(e, 0, bBufSave, iTempLength, e.Length);

                iTempLength += e.Length;

                blExistCmd = true;

                while (iTempLength >= LEN_HEAD && blExistCmd)
                {
                    if (e[0] == 5 && e[1] == 4)
                    {
                        byte[] bBufHead = new byte[LEN_HEAD];
                        Array.Copy(bBufSave, 0, bBufHead, 0, LEN_HEAD);

                        TServicePartSHS tServicePart = new TServicePartSHS();
                        object obj = tServicePart;
                        StrArr.ByteArrayToStructure(bBufHead, ref obj);
                        tServicePart = (TServicePartSHS)obj;

                        int iLengthCmd = tServicePart.Length;
                        if (iTempLength - LEN_HEAD >= iLengthCmd)
                        {
                            byte[] bBufDecode = new byte[iLengthCmd];
                            Array.Copy(bBufSave, LEN_HEAD, bBufDecode, 0, iLengthCmd);
                            DecodeCommand(tServicePart, bBufDecode);

                            Array.Reverse(bBufSave);
                            Array.Resize(ref bBufSave, bBufSave.Length - (LEN_HEAD + iLengthCmd));
                            Array.Reverse(bBufSave);

                            iTempLength -= (LEN_HEAD + iLengthCmd);

                            blExistCmd = true;

                        }
                        else
                            blExistCmd = false;
                    }
                }
            }
            catch { }
        }

        private byte[] SetServicePart(byte bCode, int iLength)
        {
            byte[] bHead = null;

            TServicePartSHS tServicePart = new TServicePartSHS();
            tServicePart.AddrSend = _addressSender;
            tServicePart.AddrReceive = _addressReceiver;
            tServicePart.Code = bCode;
            if (_counter == 255)
                _counter = 0;
            tServicePart.Counter = _counter++;
            tServicePart.Length = (byte)iLength;

            bHead = StrArr.StructToByteArray(tServicePart);

            return bHead;
        }
        #endregion

        #region SendCommand
        /// <summary>
        /// Выключить излучение
        /// </summary>
        public bool SendRadiatOff(byte letter)
        {
            var bData = new byte[1];
            bData[0] = letter;

            return WriteData(FormArrayForSend(AmpCodes.RADIAT_OFF_APP, bData));
        }

        /// <summary>
        /// Тест для Толи
        /// </summary>
        /// <param name="bData"></param>
        /// <returns></returns>
        public bool JustSendTest(byte[] bData)
        {
            return WriteData(bData);
        }

        /// <summary>
        /// Тестовая команда для Толи
        /// </summary>
        /// <returns></returns>
        public bool SendGNSS(TGnss gnss, byte power)
        {
            var bData = new byte[5];

            bData[0] = (byte)ReturnGNSSByte(gnss.gpsL1);
            bData[1] = (byte)ReturnGNSSByte(gnss.gpsL2);
            bData[2] = (byte)ReturnGNSSByte(gnss.glnssL1);
            bData[3] = (byte)ReturnGNSSByte(gnss.glnssL2);
            bData[4] = power;

            return WriteData(FormArrayForSend(AmpCodes.GNSS_APP, bData));
        }

        /// <summary>
        /// Состояние литеры
        /// </summary>
        public bool SendStatus(byte letter)
        {
            var bData = new byte[1];
            bData[0] = letter;

            return WriteData(FormArrayForSend(AmpCodes.STATUS_APP, bData));
        }

        /// <summary>
        /// Включить спуфинг с мощностью
        /// </summary>
        public bool SendSetSPOOFB(byte Power)
        {
            var bData = new byte[1];

            bData[0] = Power;

            return WriteData(FormArrayForSend(AmpCodes.SPOOF_APP, bData));
        }

        /// <summary>
        /// Запрос состояния антенны
        /// </summary>
        public bool SendStatusAntenna()
        {
            return WriteData(FormArrayForSend(AmpCodes.STATUS_ANTENNA, null));
        }

        /// <summary>
        /// Установить антенну
        /// </summary>
        public bool SendSetAntenna(AntennaCondition antenna)
        {
            var bData = new byte[1];

            bData[0] = (byte)antenna;

            return WriteData(FormArrayForSend(AmpCodes.SET_ANTENNA, bData));
        }

        /// <summary>
        /// Установить подавляемые системы навигации и включить излучение с мощностью
        /// </summary>
        public bool SendSetGNSSB(TGnss gnss, byte Power)
        {
            var bData = new byte[5];

            bData[0] = Convert.ToByte(gnss.gpsL1);
            bData[1] = Convert.ToByte(gnss.gpsL2);
            bData[2] = Convert.ToByte(gnss.glnssL1);
            bData[3] = Convert.ToByte(gnss.glnssL2);
            bData[4] = Power;

            return WriteData(FormArrayForSend(AmpCodes.GNSS_APP, bData));
        }

        #endregion

        #region CreateArrayForSend
        private byte[] FormArrayForSend(AmpCodes code, byte[] data)
        {
            if (data == null)
            {
                // create head array
                byte[] servicePart = SetServicePart((byte)code, 0);
                return servicePart;
            }
            // create head array
            byte[] head = SetServicePart((byte)code, data.Length);
            // create send array
            var bSend = head.Concat(data).ToArray();

            return bSend;
        }

        private int ReturnGNSSByte(bool GNSS)
        {
            return GNSS ? 0x11 : 0x00;
        }
        #endregion

        #region Decoder
        public void DecodeCommand(TServicePartSHS tServicePart, byte[] bData)
        {
            if (bData == null)
                return;

            byte bCodeError = bData[0];

            switch (tServicePart.Code)
            {
                case (byte)AmpCodes.STATUS_ANTENNA:
                    OnConfirmAntennaStatus?.Invoke(this, new ConfirmAntennaStatusEventArgs(bCodeError, AmpCodes.STATUS_ANTENNA, (AntennaCondition)bData[1]));
                    break;

                case (byte)AmpCodes.SET_ANTENNA:
                    OnConfirmSet?.Invoke(this, new ConfirmSetEventArgs(bCodeError, AmpCodes.SET_ANTENNA));
                    break;

                case (byte)AmpCodes.RADIAT_OFF_APP:

                    OnConfirmSet?.Invoke(this, new ConfirmSetEventArgs(bCodeError, AmpCodes.RADIAT_OFF_APP));
                    break;

                case (byte)AmpCodes.SPOOF_APP:

                    OnConfirmSet?.Invoke(this, new ConfirmSetEventArgs(bCodeError, AmpCodes.SPOOF_APP));
                    break;

                case (byte)AmpCodes.GNSS_APP:

                    OnConfirmSet?.Invoke(this, new ConfirmSetEventArgs(bCodeError, AmpCodes.GNSS_APP));
                    break;

                case (byte)AmpCodes.GNSS_APP_2:

                    OnConfirmSet?.Invoke(this, new ConfirmSetEventArgs(bCodeError, AmpCodes.GNSS_APP_2));
                    break;

                case (byte)AmpCodes.STATUS_APP:

                    int length = bData.Length;
                    if ((length-1)%6 != 0)
                        return;

                    var tParamAmp = new TParamAmp[(length - 1) / 6];

                    for (int i = 0; i < (length - 1) / 6; i++)
                    {
                        tParamAmp[i] = new TParamAmp();

                        tParamAmp[i].Snt = bData[i * 6 + 1];
                        tParamAmp[i].Rad = bData[i * 6 + 2];

                        unchecked
                        {
                            tParamAmp[i].Temp = (sbyte)(bData[i * 6 + 3]);
                        }

                        tParamAmp[i].Current = Convert.ToByte(bData[i * 6 + 4]);
                        tParamAmp[i].Power = bData[i * 6 + 5];
                        tParamAmp[i].Error = bData[i * 6 + 6];

                    }
                    
                    BitArray bits = new BitArray(new byte[] { bCodeError });
                    byte[] arrayErrors = { Convert.ToByte(bits.Get(0)), Convert.ToByte(bits.Get(1)), Convert.ToByte(bits.Get(2)), Convert.ToByte(bits.Get(3)) };

                    OnConfirmStatus?.Invoke(this, new ConfirmStatusEventArgs(arrayErrors, tParamAmp));
                    break;
            }
        }
        #endregion
    }
}
