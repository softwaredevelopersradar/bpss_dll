﻿using System;
using System.IO.Ports;
using System.Threading;

namespace BPSS_DLL
{
    public class BaseCom
    {
        #region Private
        private SerialPort _port;
        private Thread thrRead;
        protected const byte LEN_HEAD = 5;
        protected const short LEN_ARRAY = 1000;
        #endregion

        #region Events
        public event EventHandler<byte[]> OnReadByte;
        public event EventHandler<byte[]> OnWriteByte;
        public event EventHandler<bool> OnConnect;
        public event EventHandler<bool> OnDisconnect;
        #endregion

        #region Open, Close
        /// <summary>
        /// Открыть ComPort
        /// </summary>
        public bool OpenPort(string portName, Int32 baudRate, Parity parity, Int32 dataBits, StopBits stopBits)
        {
            // Open COM port
            if (_port == null)
                _port = new SerialPort();

            // if port is open
            if (_port.IsOpen)
                // close it
                ClosePort();

            // try to open 
            try
            {
                // set parameters of port
                _port.PortName = portName;
                _port.BaudRate = baudRate;
                _port.Parity = parity;
                _port.DataBits = dataBits;
                _port.StopBits = stopBits;
                // open it
                _port.Open();

                // create the thread for reading data from the port
                if (thrRead != null)
                {
                    thrRead.Abort();
                    thrRead.Join(500);
                    thrRead = null;
                }

                _port.DataReceived += new SerialDataReceivedEventHandler(p_DataReceivedHandler);

                OnConnect?.Invoke(this, true);
                return true;
            }
            catch
            {
                OnConnect?.Invoke(this, false);
                return false;
            }
        }

        /// <summary>
        /// Закрыть ComPort
        /// </summary>
        public bool ClosePort()
        {
            // clear in buffer
            try
            {
                _port.DiscardInBuffer();
                _port.DiscardOutBuffer();

                // close port
                _port.Close();

                // destroy thread of reading
                if (thrRead != null)
                {
                    thrRead.Abort();
                    thrRead.Join(500);
                    thrRead = null;
                }
                OnDisconnect?.Invoke(this, true);
                return true;
            }
            catch
            {
                OnDisconnect?.Invoke(this, false);
                return false;
            }

        }
        #endregion

        #region Write, Read
        protected bool WriteData(byte[] bSend)
        {
            try
            {
                _port.Write(bSend, 0, bSend.Length);
                OnWriteByte?.Invoke(this, bSend);
                
                return true;
            }
            catch
            {
                return false;
            }
        }


        private void p_DataReceivedHandler(object sender, SerialDataReceivedEventArgs e)
        {
            if (e.EventType == SerialData.Eof) return;
            byte[] bBufRead = new byte[LEN_ARRAY];
            try
            {
                Thread.Sleep(300);

                int iReadByte = -1;
                byte[] buffer = new byte[_port.BytesToRead];

                iReadByte = _port.Read(buffer, 0, buffer.Length);
                if (iReadByte > 0)
                {
                    OnReadByte?.Invoke(this, buffer);
                }
                iReadByte = -1;
            }
            catch
            {
            }
        }
        #endregion
    }
}