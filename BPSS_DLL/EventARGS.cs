﻿using System;

namespace BPSS_DLL
{
    public class ConfirmSetEventArgs : EventArgs
    {
        public byte CodeError { get; private set; }
        public AmpCodes AmpCode { get; private set; }

        public ConfirmSetEventArgs(byte CodeError, AmpCodes AmpCode)
        {
            this.CodeError = CodeError;
            this.AmpCode = AmpCode;
        }
    }

    public class ConfirmAntennaStatusEventArgs : EventArgs
    {
        public byte CodeError { get; private set; }
        public AntennaCondition Antenna { get; private set; }
        public AmpCodes AmpCode { get; private set; }

        public ConfirmAntennaStatusEventArgs(byte CodeError, AmpCodes AmpCode, AntennaCondition Antenna)
        {
            this.CodeError = CodeError;
            this.AmpCode = AmpCode;
            this.Antenna = Antenna;
        }
    }

    public class ConfirmStatusEventArgs : EventArgs
    {
        public byte[] CodeError { get; private set; }
        public TParamAmp[] ParamAmp { get; private set; }

        public ConfirmStatusEventArgs(byte[] CodeError, TParamAmp[] ParamAmp)
        {
            this.CodeError = CodeError;
            this.ParamAmp = ParamAmp;
        }
    }
}