﻿namespace BPSS_DLL
{
    #region Structures and Enum
    public struct TServicePartSHS
    {
        public byte AddrSend;
        public byte AddrReceive;
        public byte Code;
        public byte Counter;
        public byte Length;
    }

    public enum AmpCodes
    {
        RADIAT_OFF_APP = 13,
        STATUS_APP,
        GNSS_APP = 17,
        GNSS_APP_2,
        SPOOF_APP,
        STATUS_ANTENNA = 23,
        SET_ANTENNA = 22
    }

    public struct TParamAmp
    {
        public byte Snt;
        public byte Rad;
        public sbyte Temp;
        public byte Current;
        public byte Power;
        public byte Error;
    }

    public struct TGnss
    {
        public bool gpsL1;
        public bool gpsL2;
        public bool glnssL1;
        public bool glnssL2;
    }

    public struct BGnss
    {
        public bool BeidouL1;
        public bool BeidouL2;
        public bool GalileoL1;
        public bool GalileoL2;
    }

    public enum AntennaCondition
    {
        LOG,
        OMNI
    }
    #endregion
}
