﻿using BPSS_DLL;
using System;
using System.IO.Ports;
using System.Windows;
using System.Windows.Media;

namespace BPSS_TEST
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            InitComPort();
            InitLiterComboBox();
            InitAntennaComboBox();
        }

        BPSS bpss = new BPSS(0x04, 0x05);

        #region Init
        private void InitComPort()
        {
            foreach (string PortName in SerialPort.GetPortNames())
            {
                ComBox.Items.Add(PortName);
            }
            ComBox.SelectedIndex = 0;

            string[] listbaudrate = new string[15];
            listbaudrate[0] = "110";
            listbaudrate[1] = "300";
            listbaudrate[2] = "600";
            listbaudrate[3] = "1200";
            listbaudrate[4] = "2400";
            listbaudrate[5] = "4800";
            listbaudrate[6] = "9600";
            listbaudrate[7] = "14400";
            listbaudrate[8] = "19200";
            listbaudrate[9] = "38400";
            listbaudrate[10] = "56000";
            listbaudrate[11] = "57600";
            listbaudrate[12] = "115200";
            listbaudrate[13] = "128000";
            listbaudrate[14] = "256000";

            foreach (string baudRate in listbaudrate)
            {
                RateBox.Items.Add(baudRate);
            }
            RateBox.SelectedIndex = 6;
        }

        private void InitLiterComboBox()
        {
            for (int i = 0; i < 11; i++)
            {
                LitBoxOffRad.Items.Add(i);
                LitBoxStatus.Items.Add(i);
            }
            LitBoxOffRad.SelectedIndex = 0;
            LitBoxStatus.SelectedIndex = 0;
        }

        private void InitAntennaComboBox()
        {
            AntennaBox.Items.Add(AntennaCondition.LOG);
            AntennaBox.Items.Add(AntennaCondition.OMNI);
            AntennaBox.SelectedIndex = 0;
        }
        #endregion

        #region ButtonClick
        private void SpufingBut_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Включить спуфинг", Brushes.Red);
                bpss.SendSetSPOOFB((byte)NumericSpufing.NumValue);
            }
            catch { }
        }

        private void StatusBut_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Статус", Brushes.Red);
                bpss.SendStatus(Convert.ToByte(LitBoxStatus.SelectedItem));
            }
            catch { }
        }

        private void RadOffBut_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Выключить излучение", Brushes.Red);
                bpss.SendRadiatOff(Convert.ToByte(LitBoxOffRad.SelectedItem));
            }
            catch { }
        }

        private void NAVIBut_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Установка подавляемых систем навигации и включение излучения", Brushes.Red);

                bpss.SendSetGNSSB(new TGnss() { gpsL1 = (bool)GPS_L1.IsChecked, gpsL2 = (bool)GPS_L2.IsChecked, glnssL1 = (bool)Glonass_L1.IsChecked, glnssL2 = (bool)Glonass_L2.IsChecked }, (byte)NumericNAVI.NumValue);
            }
            catch { }
        }

        private void GNSS_TEST_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("GNSS L1", Brushes.Red);

                bpss.SendGNSS(new TGnss() { gpsL1 = (bool)GNSS_L1.IsChecked, gpsL2 = (bool)GNSS_L2.IsChecked, glnssL1 = (bool)GNSS_L1.IsChecked, glnssL2 = (bool)GNSS_L2.IsChecked }, (byte)NumericNAVI.NumValue);
            }
            catch { }
        }

        private void AntennaBut_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Установить антенну", Brushes.Red);

                bpss.SendSetAntenna((AntennaCondition)AntennaBox.SelectedItem);
            }
            catch { }
        }

        private void AntennaStatusBut_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Запрос состояния антенны", Brushes.Red);

                bpss.SendStatusAntenna();
            }
            catch { }
        }

        private void SendButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("JustSend", Brushes.Red);

                string byteString = JustSend.Text.Replace(" ", "");

                byte[] array = new byte[byteString.Length / 2];

                for (int i = 0; i < byteString.Length; i += 2)
                {
                    array[i / 2] = Convert.ToByte(byteString.Substring(i, 2), 16);
                }

                bpss.JustSendTest(array);
            }
            catch(Exception ex)
            {
                Display.AddTextToLog(ex.ToString());
            }
        }

        private void ConnectBut_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var a = bpss.OpenPort(ComBox.SelectedItem.ToString(), Convert.ToInt32(RateBox.SelectedItem), Parity.None, 8, StopBits.One);
                if (a == true)
                    Display.AddTextToLog("Порт открыт", Brushes.Red);
                else
                    Display.AddTextToLog("Ошибка подключения", Brushes.Red);
                bpss.OnReadByte += Bpps_OnReadByte;
                bpss.OnWriteByte += Bpps_OnWriteByte;
                bpss.OnConfirmSet += Bpps_OnConfirmSet;
                bpss.OnConfirmStatus += Bpps_OnConfirmStatus;
                bpss.OnConfirmAntennaStatus += Bpps_OnConfirmAntennaStatus;
            }
            catch
            {

            }
        }

        private void DisconnectBut_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var a = bpss.ClosePort();
                if (a == true)
                    Display.AddTextToLog("Порт закрыт", Brushes.Red);
                else
                    Display.AddTextToLog("Ошибка закрытия порта", Brushes.Red);
                bpss.OnReadByte -= Bpps_OnReadByte;
                bpss.OnWriteByte -= Bpps_OnWriteByte;
                bpss.OnConfirmSet -= Bpps_OnConfirmSet;
                bpss.OnConfirmStatus -= Bpps_OnConfirmStatus;
                bpss.OnConfirmAntennaStatus -= Bpps_OnConfirmAntennaStatus;
            }
            catch
            {

            }
        }
        #endregion

        #region Event`s
        private void Bpps_OnConfirmStatus(object sender, ConfirmStatusEventArgs e)
        {
            if (Display.ShowAllByte == false)
            {
                Dispatcher.Invoke(() => { Display.AddTextToLog($"Cтатус", Brushes.Green); });
                Dispatcher.Invoke(() => { Display.AddTextToLog($"Коды ошибки: Литера 1: {e.CodeError[0]}\nЛитера 2: {e.CodeError[1]}\nЛитера 3: {e.CodeError[2]}\nЛитера 4: {e.CodeError[4]}", Brushes.Green); });
            }
        }

        private void Bpps_OnConfirmSet(object sender, ConfirmSetEventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                {
                    Dispatcher.Invoke(() => { Display.AddTextToLog($"Команда: {e.AmpCode}", Brushes.Green); });
                    Dispatcher.Invoke(() => { Display.AddTextToLog($"Код ошибки: {e.CodeError}", Brushes.Green); });
                }
            }
            catch { }
        }

        private void Bpps_OnConfirmAntennaStatus(object sender, ConfirmAntennaStatusEventArgs e)
        {
            if (Display.ShowAllByte == false)
            {
                Dispatcher.Invoke(() => { Display.AddTextToLog($"Команда: {e.AmpCode}", Brushes.Green); });
                Dispatcher.Invoke(() => { Display.AddTextToLog($"Состояние антенны: {e.Antenna}", Brushes.Green); });
                Dispatcher.Invoke(() => { Display.AddTextToLog($"Код ошибки: {e.CodeError}", Brushes.Green); });
            }
        }

        private void Bpps_OnWriteByte(object sender, byte[] e)
        {
            try
            {
                string a = "";
                foreach (var i in e)
                {
                    a += i.ToString("X2");
                    a += " ";
                }
                Display.AddTextToLog(a, Brushes.Red);
            }
            catch { }
        }

        private void Bpps_OnReadByte(object sender, byte[] e)
        {
            try
            {
                string a = "";
                foreach (var i in e)
                {
                    a += i.ToString("X2");
                    a += " ";
                }
                Dispatcher.Invoke(() => { Display.AddTextToLog(a, Brushes.Green); });
            }
            catch
            {

            }
        }
        #endregion
    }
}
