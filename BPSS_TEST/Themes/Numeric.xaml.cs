﻿using System.Windows;
using System.Windows.Controls;

namespace BPSS_TEST
{
    /// <summary>
    /// Логика взаимодействия для Numeric.xaml
    /// </summary>
    public partial class Numeric : UserControl
    {
        public Numeric()
        {
            InitializeComponent();
            txtNum.Text = _numValue.ToString();
        }

        private int _numValue = 0;

        public int NumValue
        {
            get { return _numValue; }
            set
            {
                _numValue = value;

                if (_numValue > 127)
                {
                    _numValue = 127;
                    value = 127;
                }
                else if (_numValue < 0)
                {
                    _numValue = 0;
                    value = 0;
                }

                txtNum.Text = value.ToString();
            }
        }

        private void cmdUp_Click(object sender, RoutedEventArgs e)
        {
            NumValue++;
        }

        private void cmdDown_Click(object sender, RoutedEventArgs e)
        {
            NumValue--;
        }

        private void txtNum_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (txtNum == null)
            {
                return;
            }

            if (!int.TryParse(txtNum.Text, out _numValue))
                txtNum.Text = _numValue.ToString();
        }
    }
}
